import { Container, Typography } from '@mui/material';
import { Link } from 'react-router-dom';

const NotFound = () => {
  return (
    <Container maxWidth="sm" style={{ textAlign: 'center', paddingTop: '2rem', maxWidth: '1000px' }}>
      <Typography variant="h1">404 Not Found</Typography>
      <Typography variant="h5">Страница не найдена</Typography>
      <Typography variant="body1">
        Вернуться на <Link to="/">главную страницу</Link>.
      </Typography>
    </Container>
  );
};

export default NotFound;
