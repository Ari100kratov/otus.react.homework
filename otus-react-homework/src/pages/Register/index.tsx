import React, { useState } from 'react';
import { Button, Container, CssBaseline, TextField, Typography } from '@mui/material';
import { Send } from '@mui/icons-material';
import { connect } from 'react-redux';
import { registerAction } from '../../Auth/authActions';

interface FormStyles {
  paper: React.CSSProperties;
  form: React.CSSProperties;
  submit: React.CSSProperties;
}

const formStyles: FormStyles = {
  paper: {
    marginTop: '8px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%',
    marginTop: '1rem',
  },
  submit: {
    margin: '1rem 0 2rem',
  },
};

interface RegisterProps {
  register: (username: string) => void;
}

function Register({
  register
}: RegisterProps) {

  const [formData, setFormData] = useState({
    username: '',
    password: '',
    confirmPassword: ''
  });

  const [passwordError, setPasswordError] = useState('');

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { username, password, confirmPassword } = formData;

    if (password !== confirmPassword) {
      setPasswordError('Пароль и подтверждение пароля не совпадают');
      return;
    }

    setPasswordError('');

    // делаем вид, что регистрация прошла успешно и просто производим авторизцию
    register(username);
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div style={formStyles.paper}>
        <Typography variant="h5">Регистрация</Typography>
        <form style={formStyles.form} onSubmit={handleSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            label="Имя пользователя"
            name="username"
            value={formData.username}
            onChange={handleChange}
            required
          />
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            label="Пароль"
            type="password"
            name="password"
            value={formData.password}
            onChange={handleChange}
            required
          />
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            label="Подтверждение пароля"
            type="password"
            name="confirmPassword"
            value={formData.confirmPassword}
            onChange={handleChange}
            required
          />
          {passwordError && (
            <Typography variant="body2" color="error">
              {passwordError}
            </Typography>
          )}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            style={formStyles.submit}
            startIcon={<Send />}
          >
            Зарегистрироваться
          </Button>
        </form>
      </div>
    </Container >
  );
}

const mapDispatchToProps = {
  register: registerAction
};

export default connect(null, mapDispatchToProps)(Register);
