import { Typography, Container, Button } from '@mui/material';
import { connect } from 'react-redux';
import { RootState } from '../../store';
import { ExitToApp } from '@mui/icons-material';
import { logoutAction } from '../../Auth/authActions';

interface HomePageProps {
    username: string | undefined,
    logout: () => void;
}

function HomePage({ username, logout }: HomePageProps) {
    return (
        <Container maxWidth="md">
            <div style={{ textAlign: 'center', marginTop: '2rem' }}>
                <Typography variant="h3">
                    Добро пожаловать, {username}!
                </Typography>
                <Button
                    variant="contained"
                    color="primary"
                    startIcon={<ExitToApp />}
                    onClick={logout}
                >
                    ВЫЙТИ
                </Button>
            </div>
        </Container>
    );
};

const mapStateToProps = (state: RootState) => ({
    username: state.auth.user?.username,
});

const mapDispatchToProps = {
    logout: logoutAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
