import React, { useState } from 'react';
import { Button, Container, CssBaseline, TextField, Typography } from '@mui/material';
import { Send } from '@mui/icons-material';
import { connect } from 'react-redux';
import { loginAction } from '../../Auth/authActions';
import { RootState } from '../../store';
import { Link } from 'react-router-dom';

interface FormStyles {
  paper: React.CSSProperties;
  form: React.CSSProperties;
  submit: React.CSSProperties;
  testLoginInfo: React.CSSProperties;
}

const formStyles: FormStyles = {
  paper: {
    marginTop: '8px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%',
    marginTop: '1rem',
  },
  submit: {
    margin: '1rem 0 2rem',
  },
  testLoginInfo: {
    marginTop: '1rem', // Расположение приписки снизу от кнопки
    fontSize: '14px',
    color: 'gray',
  },
};

interface LoginProps {
  error: string,
  login: (username: string, password: string) => void;
}

function Login({
  error,
  login
}: LoginProps) {

  const [formData, setFormData] = useState({ username: '', password: '' });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { username, password } = formData;
    login(username, password);
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div style={formStyles.paper}>
        <Typography variant="h5">Авторизация</Typography>
        <form style={formStyles.form} onSubmit={handleSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            label="Имя пользователя"
            name="username"
            value={formData.username}
            onChange={handleChange}
            required
          />
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            label="Пароль"
            type="password"
            name="password"
            value={formData.password}
            onChange={handleChange}
            required
          />
          {error && (
            <Typography variant="body2" color="error">
              {error}
            </Typography>
          )}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            style={formStyles.submit}
            startIcon={<Send />}
          >
            Войти
          </Button>
          <Typography variant="body2">
            У вас нет аккаунта? <Link to="/register">Зарегистрируйтесь</Link>
          </Typography>
          <Typography variant="body2">
            Может вы хотите посмотреть <Link to="/404">404 not found</Link>?
          </Typography>
          <Typography style={formStyles.testLoginInfo}>
            *Попробуйте ввести admin и 123 :D
          </Typography>
        </form>
      </div>
    </Container >
  );
}

const mapStateToProps = (state: RootState) => ({
  error: state.auth.error,
});

const mapDispatchToProps = {
  login: loginAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);