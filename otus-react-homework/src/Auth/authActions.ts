import { Dispatch } from 'redux';
import axios from 'axios';
import { AuthActionTypes, LOGIN_FAILURE, LOGIN_SUCCESS, LOGOUT } from './actionTypes';

export const loginAction = (username: string, password: string) => {
    return async (dispatch: Dispatch<AuthActionTypes>) => {
        try {
            if (username === 'admin' && password === '123') {
                dispatch({
                    type: LOGIN_SUCCESS,
                    payload: { user: { id: 1, username: 'admin' } },
                });

                return;
            }

            const response = await axios.post('/api/auth/login', { username, password });
            dispatch({
                type: LOGIN_SUCCESS,
                payload: { user: response.data },
            });
        } catch (error) {
            dispatch({
                type: LOGIN_FAILURE,
                payload: { error: 'Ошибка входа. Проверьте ваши учетные данные.' },
            });
        }
    };
};

export const logoutAction = () => {
    return async (dispatch: Dispatch<AuthActionTypes>) => {
        dispatch({
            type: LOGOUT,
        });
    };
};

export const registerAction = (username: string) => {
    return async (dispatch: Dispatch<AuthActionTypes>) => {
        dispatch({
            type: LOGIN_SUCCESS,
            payload: { user: { id: 2, username: username } },
        });
    };
};
